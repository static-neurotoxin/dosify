#include <cctype>
#include <iostream>
#include <vector>
#include <codecvt>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>


const std::u32string problemChars(U"\\/<>:\"|?*");
const std::u32string substitutionTable(U"␀␁␂␃␄␅␆␇␈␉␊␋␌␍␎␏␐␑␒␓␔␕␖␗␘␙␚␛␜␝␞␟　！＂＃＄％＆＇（）＊＋，－．／０１２３４５６７８９：；＜＝＞？＠ＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ［＼］＾＿｀ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚ｛｜｝～␡");

enum dosify_options {reserved, symbols, ascii};

struct dosifier
{
    dosifier() : m_recursive(false), m_verbose(false), m_confirm(true), m_mode(reserved) {}

    boost::filesystem::path dosify(const boost::filesystem::path &filename)
    {
        std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> cvt;

        // Process filename as utf32
        std::u32string normalizedFilename;

        for (char32_t c: cvt.from_bytes(filename.string()))
        {
            bool makeWide(std::iscntrl(c));
            if (!makeWide)
            {
                switch (m_mode)
                {
                    case ascii:
                        makeWide = std::isprint(c);
                        break;

                    case symbols:
                        makeWide = std::ispunct(c);
                        break;

                    case reserved:
                        makeWide = (problemChars.find(c) != std::string::npos);
                        break;
                }
            }
            if (makeWide && c != U'.')
            {
                normalizedFilename.push_back(substitutionTable[c]);
            }
            else
            {
                normalizedFilename.push_back(c);
            }
        }

        return cvt.to_bytes(normalizedFilename);
    }

    void processPath(const boost::filesystem::path &path)
    {
        if (!boost::filesystem::is_directory(path))
        {
            std::cerr << "Unable to scan " << path << std::endl;
            std::cerr << "Not a directory" << std::endl;
        }
        else
        {
            if (m_verbose)
            {
                std::cout << "Processing " << path << std::endl;
            }

            for (auto entry: boost::make_iterator_range(boost::filesystem::recursive_directory_iterator(path), {}))
            {
                boost::filesystem::path filename(entry.path().filename());
                boost::filesystem::path newFilename(dosify(filename));
                if (newFilename != filename)
                {
                    std::cout << "Renaming " << path / filename << " to " << path / newFilename;
                    if (m_confirm)
                    {
                        std::cout << " [y/n]?" << std::endl;
                        char c;
                        std::cin.get(c);
                        if (std::tolower(c) != 'y')
                            continue;
                    } else
                        std::cout << std::endl;

                    entry.replace_filename(path / newFilename);
                } else if (m_verbose)
                {
                    std::cout << "Scanning " << path / filename << std::endl;
                }

                if (m_recursive && boost::filesystem::is_directory(path / entry.path()))
                {
                    processPath(path / entry.path());
                }
            }
        }
    }

    bool m_recursive;
    bool m_verbose;
    bool m_confirm;

    dosify_options m_mode;
};

int main(int argc, const char *argv[])
{
    dosifier d;

    bool showHelp;
    std::string mode;

    std::vector<boost::filesystem::path> paths;

    boost::program_options::options_description description;
    description.add_options()
            ("help,?",      boost::program_options::value(&showHelp)->implicit_value(true)->zero_tokens(),       "Show program options")
            ("mode,m",      boost::program_options::value(&mode),                                                "Replacement mode (reserved|symbols|ascii)")
            ("recursive,r", boost::program_options::value(&d.m_recursive)->implicit_value(true) ->zero_tokens(), "Recursively rename files")
            ("verbose,v",   boost::program_options::value(&d.m_verbose)  ->implicit_value(true) ->zero_tokens(), "Display extra progress messages")
            ("confirm,y",   boost::program_options::value(&d.m_confirm)  ->implicit_value(false)->zero_tokens(), "Don't prompt when renaming files")
            ("input,I",     boost::program_options::value(&paths),                                               "Input files");

    boost::program_options::positional_options_description p;
    p.add("input", -1);

    boost::program_options::variables_map options;
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
                            .options(description)
                            .positional(p)
                            .run(), options);

    boost::program_options::notify(options);

    for(char &c: mode)
    {
        c = std::tolower(c);
    }

    if(mode == "reserved" || mode == "r")
        d.m_mode = reserved;
    else if(mode == "symbols" || mode == "s")
        d.m_mode = symbols;
    else if(mode == "ascii" || mode == "a")
        d.m_mode = ascii;

    if(showHelp || paths.empty())
    {
        std::cout << "Dosify" << std::endl << std::endl;
        std::cout << "Replace problematic characters in filenames to unicode wide characters" << std::endl << std::endl;
        std::cout << description << std::endl;
        exit(0);
    }

    for(const boost::filesystem::path &path: paths)
    {
        d.processPath(path);
    }

    return 0;
}